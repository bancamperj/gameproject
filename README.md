# Desolate Island #

A text adventure game where you must find materials to make it off the Island

## Requirements ##

[Haskell Platform](https://www.haskell.org/downloads#minimal)

## Compile and Run ##

>`$ ghc textGame.hs`

>`$ ./textGame`