import System.IO
import Data.List
import Matrices

data Player = Player {
	name :: String, 
	health :: Int,
	currLoc :: Int,
	inv :: [Item]
}

instance Show Player where
	show (Player n h cl i) = "\n" ++ n ++ ": " ++ (show h) ++ "HP\nCurrent Items: " ++ (intercalate ", " (map show i)) 

data World =  World{
	locations :: [Location],
	navmatrix :: NavMatrix
}

data State = Alive {
	player :: Player,
	world :: World
} | Dead | Win

--Print intro to screen
intro :: IO()
intro = putStrLn $ "\nYou have just CRASH landed in the middle of an island.You wake up\n"++
					"to the burning smell of metals and flesh as you begin to see the devastating ruins of what was the\n"++
					"plane you were just flying in. You try to move, but your leg is badly hurt, and losing a lot of blood. Use\n"++
					"your last bit of strength to find items that can help you get off the island\n"

outro :: IO()
outro = putStrLn $ "\n\nThanks for playing"

--Create the connections and locations regarding the back story
createWorld :: IO World
createWorld = do
	nav <- makeMap
	locs <- makeLocs
	return $ World locs nav

-- Determines whether the game ends or not
inGame :: State -> IO()
inGame Dead = putStrLn $ ""
inGame (Alive p w) = putStrLn $ (show p) ++ (show ((locations w) !! (currLoc p)))


--After the intro, asks for user input to start the game
startGame :: IO State
startGame = do
	hPutStr stderr "\nWelcome to Desolate Island!\n\nPlease enter your name: "
	name <- getLine
	world <- createWorld
	return $ Alive (Player name 100 0 []) world

--Allowing the player to move throughout the map
doSomething :: State -> IO Char
doSomething (Alive p w) = do
	hPutStr stderr "\nMove [N]orth, [S]outh, [E]ast, [W]est. [P]ick up item. Print [M]ap (only if in inventory)\n\n[Q]uit game\n\nWhat would you like to do: "
	fmap head getLine

--Determines location, items, and state of player
updateGame :: State -> Char -> IO State
updateGame Dead _ = return $ Dead
updateGame Win _ = return $ Win
updateGame (Alive p w) c
	|(c == 'N' || c == 'n') = checkLoc (Alive p w) North
	|(c == 'S' || c == 's') = checkLoc (Alive p w) South
	|(c == 'E' || c == 'e') = checkLoc (Alive p w) East
	|(c == 'W' || c == 'w') = checkLoc (Alive p w) West
	|(c == 'P' || c == 'p') = pickUp (Alive p w)
	|(c == 'Q' || c == 'q') = return $ Dead
	|otherwise = if ((health p) - 4) <= 0
		then do
			putStrLn $ "You've bled out."
			return $ Dead
		else do	
			return $ (Alive p{health = (health p) - 4} w)

--Moves the player to a valid location
checkLoc :: State -> Dir -> IO State
checkLoc (Alive p w) dir = if (fromto (currLoc p) dir (navmatrix w)) == -1
	then do
		if ((health p) - 4) <= 0
			then do
				putStrLn $ "You've bled out."
				return $ Dead
		else do
			putStrLn $ "\nNo location to the " ++ (show dir) ++ "\n"
			return $ (Alive p{health = ((health p) - 4)} w)
	else do
		if ((health p) - 4) <= 0
			then do
				putStrLn $ "You've bled out."
				return $ Dead
		else do
			return $ Alive (Player (name p) ((health p) - 4) (fromto (currLoc p) dir (navmatrix w)) (inv p)) w


--Prints the map if they have it in their inventory
printMap :: State -> IO State
printMap (Alive p w) = if (Item "Map" "") `elem` (inv p)
	then do
		putStrLn $ ""
		return $ (Alive p w)
	else do
		putStrLn $ "\nNo map in inventory\n"
		return $ (Alive p w)

--Appends item to player inv, and takes away item from location
pickUp :: State -> IO State
pickUp (Alive p w) = if (locItem ((locations w) !! (currLoc p))) == (Item "" "")
	then do
		if ((health p) - 4) <= 0
			then do
				putStrLn $ "You've bled out."
				return $ Dead
		else do
			putStrLn $ "\nNo item here. You're losing blood!\n"
			return $ Alive (Player (name p) ((health p) - 4) (currLoc p) (inv p)) w
	else do
		putStrLn $ "\n" ++ (itemName (locItem ((locations w) !! (currLoc p)))) ++ " picked up.\n"
		return $ Alive (p{inv = (inv p) ++ [(locItem ((locations w) !! (currLoc p)))]}) (rmItem w (currLoc p))

--Removes item from location in world
rmItem :: World -> Int -> World
rmItem w loc = w' where
	(prevLocs, targLoc:succLocs) = splitAt loc (locations w)
	w' = World (prevLocs ++ [(Location (locName targLoc) (locDesc targLoc) (locId targLoc) (Item "" ""))] ++ succLocs) (navmatrix w)

rmItemFromInv :: [Item] -> Maybe Int -> [Item]
rmItemFromInv inv Nothing = inv
rmItemFromInv inv (Just index) = inv' where
	(prevItems, targItem:succItems) = splitAt index inv
	inv' = prevItems ++ succItems

--Situational location story lines
updateLocs :: State -> IO State
updateLocs Dead = return $ Dead
updateLocs (Alive (Player n h 4 inv) w) = if (Item "Gun" "" `elem` inv)
	then do
		hPutStr stderr "\nYou've been attacked by a jaguar but you were able to use your Gun to scare it away"
		return $ Alive (Player n h 4 inv) w
	else if Item "Knife" "" `elem` inv
		then do
			hPutStr stderr "\nYou've been attacked by a jaguar but you were able to use your Knife to scare it away"
			return $ Alive (Player n h 4 inv) w
	else do
		hPutStr stderr "\nYou've been attacked by a jaguar. You managed to scare it away with a loose branch, but you're badly hurt"
		return $ Alive (Player n (h - 16) 4 inv) w
updateLocs (Alive (Player n h 7 inv) w) = if (Item "Gun" "" `elem` inv)
	then do
		hPutStr stderr "\nYou were able to shoot some snakes and scare the rest away"
		return $ Alive (Player n h 7 inv) w
	else if (Item "Knife" "" `elem` inv)
		then do
			hPutStr stderr "\nYou were able to slice through some snakes and scare the rest away"
			return $ Alive (Player n h 7 inv) w
	else do
		hPutStr stderr "\nYou had nothing to protect yourself with against the snakes, and you have died"
		return $ Dead
updateLocs (Alive (Player n h 13 inv) w) = if (Item "Pole" "" `elem` inv && Item "Wood" "" `elem` inv && Item "Cloth" "" `elem` inv && Item "Rope" "" `elem` inv)
	then do
		hPutStr stderr "\nYou were able to make a boat by binding the wood with the rope, \nusing the pole as a mast with the cloth as a sail. You're off the island and sailing to freedom\n\n"
		return $ Win
	else if (Item "Raft" "" `elem` inv && Item "Pole" "" `elem` inv)
		then do
			hPutStr stderr "\nYou were able to blow up the raft and use the pole as an oar. You're off the island and sailing to freedom!\n\n"
			return $ Win
	else do
		hPutStr stderr "\nThis would be the perfect place to sail off, but it seems like you dont have enough items\nCheck back at dufferent locations to see if you can find things to fashion together a boat"
		return $ Alive (Player n h 13 inv) w
updateLocs (Alive p w) = return $ Alive p w

--Situational items
updateItems :: State -> IO State
updateItems (Alive (Player n h l inv) w) = if Item "Rag" "" `elem` inv
	then do
		return $ Alive (Player n (h + 8) l (rmItemFromInv inv (elemIndex (Item "Rag" "") inv))) w
	else do
		return $ Alive (Player n h l inv) w

--Looping through each turn
gameLoop :: State -> IO()
gameLoop Dead = return ()
gameLoop Win = return ()
gameLoop st = do
	inGame st
	c <- doSomething st
	st' <- updateItems st
	st'' <- updateLocs st'
	newSt <- updateGame st'' c
	gameLoop newSt

main = do
	player <- startGame
	intro
	gameLoop player
	outro

