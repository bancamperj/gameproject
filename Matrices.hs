module Matrices where

data Dir = North | East | South | West | None deriving (Show, Eq)

opposite :: Dir -> Dir
opposite dir
	|dir == North = South
	|dir == East = West
	|dir == South = North
	|dir == West = East
	|otherwise = None

type Matrix a = [[a]]

type LocId = Int

data Location = Location {
	locName :: String,
	locDesc :: String,
	locId :: Int,
	locItem :: Item
}

instance Show Location where
	show (Location n d locId li) = "\n\nCurrent Location: "++n++"\nItem in "++n++": "++(show li)++"\n\n"++d

data Item = Item {
	itemName :: String,
	itemDesc :: String
}

instance Show Item where
	show (Item "" "") = ""
	show (Item n d) = n++" - "++d

instance Eq Item where
	i1 == i2 = (itemName i1) == (itemName i2)

getCell :: (Int, Int) -> Matrix a -> a
getCell (row, col) = (!! col) . (!! row)

setElem :: Int -> a -> [a] -> [a]
setElem i x xs = prec++x:succ where
    (prec, _:succ) = splitAt i xs

setCell :: (Int,Int) -> a -> Matrix a -> Matrix a
setCell (row,col) x xs = setElem row (setElem col x (xs !! row)) xs

data Connections = Connections { 
	north :: LocId,
	east  :: LocId,
    south :: LocId,
    west  :: LocId 
} deriving (Show,Eq)

newtype NavMatrix = NavMatrix { 
	conns :: [Connections]
} deriving Eq

instance Show NavMatrix where
    show (NavMatrix m) = concat $ map (\x -> (show x)++"\n") m

connectTo :: Dir -> LocId -> Connections -> Connections
connectTo North to row = row{north=to}
connectTo South to row = row{south=to}
connectTo East  to row = row{east =to}
connectTo West  to row = row{west =to}

connect :: Dir -> LocId -> LocId -> NavMatrix -> NavMatrix
connect dir from to (NavMatrix mat) = NavMatrix mat'' where
    (precRows,targRow:succRows) = splitAt from mat
    newConn = connectTo dir to targRow
    mat' = precRows ++ newConn:succRows
    (precRows',targRow':succRows') = splitAt to mat'
    newConn' = connectTo (opposite dir) from targRow'
    mat'' = precRows' ++ newConn':succRows'

fromto :: LocId -> Dir -> NavMatrix -> LocId
fromto from dir = getdir . (!! from) . conns where
    getdir = case dir of North -> north
                         East  -> east
                         South -> south
                         West  -> west

no = (-1)
unconnected = Connections no no no no

emptyMap :: Int -> NavMatrix
emptyMap nlocs = NavMatrix $ take nlocs $ repeat unconnected

--Connects all the map locations
makeMap :: IO NavMatrix
makeMap = return $ ((connect East 0 1) .
	(connect North 1 2) . 
	(connect South 1 3) . 
	(connect West 0 4) . 
	(connect North 4 8) . 
	(connect West 8 9) . 
	(connect North 9 10) . 
	(connect West 4 5) . 
	(connect West 5 6) . 
	(connect South 6 7) . 
	(connect South 5 11) . 
	(connect South 11 12) . 
	(connect East 12 13) . emptyMap) 14

--Make the list 
makeLocs :: IO [Location]
makeLocs = return $ [Location "Edge of Jungle" start 0 (Item "" ""),
	Location "Plane" plane 1 (Item "Rag" "Stops bleeding for 2 turns"),
	Location "Cockpit" cockpit 2 (Item "Gun" "Able to shoot threats"),
	Location "Plane Lavatory" lavatory 3 (Item "Knife" "Able to kill threats"),
	Location "Jungle" jungle 4 (Item "Rag" "Stops your bleeding leg for 2 turns"),
	Location "Jungle" jungle' 5 (Item "Cloth" "Big sheet-like blanket, can be used for a sail"),
	Location "Cave" cave 6 (Item "Rope" "Bind things together"),
	Location "Snake Pit" pit 7 (Item "Map" "Able to see all locations"),
	Location "Lake" lake 8 (Item "Pole" "Strong metal pole, can be used as a mast"),
	Location "Shore" shore 9 (Item "Rag" "Stops your bleeding leg for 2 turns"),
	Location "Camp" camp 10 (Item "Raft" "Blow up boat"),
	Location "Jungle" jungle'' 11 (Item "Wood" "5 broken pieces flat wood"),
	Location "Swamp" swamp 12 (Item "Rag" "Stops your bleeding leg for 2 turns"),
	Location "Beach" beach 13 (Item "" "")]

--Descriptions of locations
start :: String
start = "There are dense trees in front of you. You see the wreckage of the plane to your right,\n"++
	"but you're able to explore what's inside... To your left is the deep dark Jungle."

plane :: String
plane = "Limbs, bodies, and blood fill the seats and walls of the plane. No one else\n"++
	"is alive around you. You look around and see things you can use.\n"++
	"The aisles are barely blocked by knocked over chairs and bodies. Explore\n"++
	"both ends of the plane, or go back outside."

cockpit :: String
cockpit = "Both pilots are still and bloodied from the crash. Return back to the middle of the plane to exit."

lavatory :: String
lavatory = "This is the only clean and intact place inside the plane. Head back to the middle of the plane to exit."

jungle :: String
jungle = "The trees and vines are so dense you can barely make your way through.\n"++
	"There are many noises around you, listening carefully for any person that could\n"++
	"still be alive. Keep going deeper into the jungle or move up to a clearing you see\n"++
	"in the distance"

jungle' :: String
jungle' = "The trees and vines start to clear to your left. Continue going deeper into the jungle, down"

cave :: String
cave = "Surrounded by old boulders, an opening through them can fit one person.\n"++
	"Cave drawings and cryptic messages decor the walls of the cave. The cave seems to go\n"++
	"deeper, or head back out into the jungle"

pit :: String
pit = "You've fallen into a snake pit! Venomous snakes start to surround you as you try to\n"++
	"look around to find a way out. Only way out is the way you came in, UP"

lake :: String
lake = "An open area with a lake that leads to more dense trees. A shore is to your left or\n"++
	"head back down into the jungle"

shore :: String
shore = "The lake in the distance and you see wreckage of a tent and some firewood that could've been\n"++
	"used some time ago. There could be something in what looks to be a campsite. Check it out or head back to the lake"

camp :: String
camp = "Remnants of bones and firewood are in a pile. People were here a very long time ago, but it looks like\n"++
	"dead end. Maybe there's something here you can use"

jungle'' :: String
jungle'' = "Dense trees and vines build up again, but you can see them clearing up ahead\n"

swamp :: String
swamp = "Thre trees have cleared completely, you see water to the right of you"

beach :: String
beach = "This could be your perfect escape. White sands, clear waters and nothing but water for miles\n"++
	"There seems to be a boat in the distance if only there was a way to get there"


